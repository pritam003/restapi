package com.qa.base;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class TestBase {
	
	public int RESPONSE_STATUS_CODE_200 = 200;//OK
	public int RESPONSE_STATUS_CODE_500 = 500;//Internal Server Error
	public int RESPONSE_STATUS_CODE_400 = 400;//Bad Request
	public int RESPONSE_STATUS_CODE_401 = 401;//Unauthorized Error
	public int RESPONSE_STATUS_CODE_201 = 201;//Created
	
	public Properties prop;
		
	public TestBase() {
		try { 
			prop = new Properties();
			FileInputStream ip = new FileInputStream(System.getProperty("user.dir")+"/src/main/java/com/qa/config/config.properties");
			//FileInputStream ip = new FileInputStream("D:\\my_workspace\\restapi\\src\\main\\java\\com\\qa\\config\\config.properties");
			prop.load(ip);  
			} catch(FileNotFoundException e) {
			e.printStackTrace();
		} catch(IOException e) {
			e.printStackTrace();
		}
	}
}
