package com.qa.test;

import java.io.IOException;
import java.util.HashMap;

import org.apache.http.Header;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.qa.base.TestBase;
import com.qa.client.RestClient;
import com.qa.util.TestUtil;

public class GetAPITest extends TestBase {
	
	TestBase testBase;
	String serviceUrl;
	String apiUrl;
	String url;
	RestClient restClient;
	CloseableHttpResponse closeableHttpResponse;
	
	@BeforeMethod
	public void setUp() {
		testBase = new TestBase();
		serviceUrl = prop.getProperty("URL");
		apiUrl = prop.getProperty("serviceURL");
		//https://reqres.in/api/users
		
		url = serviceUrl + apiUrl;
	}
	
	@Test(priority=1)
	public void getAPITestWithoutHeaders() throws ClientProtocolException, IOException {
		restClient = new RestClient();
		closeableHttpResponse = restClient.get(url);
		
		//a. Status Code
		int statusCode = closeableHttpResponse.getStatusLine().getStatusCode();
		System.out.println("Status Code----->" +statusCode);
		
		Assert.assertEquals(statusCode, RESPONSE_STATUS_CODE_200, "Status code is not 200");
		
		//b. JSON String
		String responseString = EntityUtils.toString(closeableHttpResponse.getEntity(), "UTF-8");
		
		JSONObject responsejson = new JSONObject(responseString);
		System.out.println("Response JSON from API--->" +responsejson);
		
		//Single value assertion:
		//per page:
		String perPageValue = TestUtil.getValueByJPath(responsejson, "/per_page");
		System.out.println("Value of per page is--->"+ perPageValue);
		
		Assert.assertEquals(Integer.parseInt(perPageValue), 3);
		
		//total:
		String totalValue = TestUtil.getValueByJPath(responsejson, "/total");
		System.out.println("Value of total is--->"+ totalValue);
		
		Assert.assertEquals(Integer.parseInt(totalValue), 12);
		
		//get the value from JSON ARRAY
		String lastName = TestUtil.getValueByJPath(responsejson, "/data[0]/last_name");
		String id = TestUtil.getValueByJPath(responsejson, "/data[0]/id");
		String avatar = TestUtil.getValueByJPath(responsejson, "/data[0]/avatar");
		String firstName = TestUtil.getValueByJPath(responsejson, "/data[0]/first_name");
		
		System.out.println(lastName);
		System.out.println(id);
		System.out.println(avatar);
		System.out.println(firstName);
		
		Assert.assertEquals(lastName, "Bluth");
		Assert.assertEquals(Integer.parseInt(id), 1);
		Assert.assertEquals(avatar, "https://s3.amazonaws.com/uifaces/faces/twitter/calebogden/128.jpg");
		Assert.assertEquals(firstName, "George");
		
		//c. All Headers
		Header[] headerArray = closeableHttpResponse.getAllHeaders();
		HashMap<String, String> allHeader = new HashMap<String, String>();
		for(Header header : headerArray) {
			allHeader.put(header.getName(), header.getValue());
		}
		System.out.println("Headers Arrays--->" +allHeader);
	}
	
	@Test(priority=2)
	public void getAPITestWithHeaders() throws ClientProtocolException, IOException {
		restClient = new RestClient();
		
		HashMap<String, String> headerMap = new HashMap<String, String>();
		headerMap.put("Content-Type", "application/json"); 
		
		closeableHttpResponse = restClient.get(url, headerMap);
		
		//a. Status Code
		int statusCode = closeableHttpResponse.getStatusLine().getStatusCode();
		System.out.println("Status Code----->" +statusCode);
		
		Assert.assertEquals(statusCode, RESPONSE_STATUS_CODE_200, "Status code is not 200");
		
		//b. JSON String
		String responseString = EntityUtils.toString(closeableHttpResponse.getEntity(), "UTF-8");
		
		JSONObject responsejson = new JSONObject(responseString);
		System.out.println("Response JSON from API--->" +responsejson);
		
		//Single value assertion:
		//per page:
		String perPageValue = TestUtil.getValueByJPath(responsejson, "/per_page");
		System.out.println("Value of per page is--->"+ perPageValue);
		
		Assert.assertEquals(Integer.parseInt(perPageValue), 3);
		
		//total:
		String totalValue = TestUtil.getValueByJPath(responsejson, "/total");
		System.out.println("Value of total is--->"+ totalValue);
		
		Assert.assertEquals(Integer.parseInt(totalValue), 12);
		
		//get the value from JSON ARRAY
		String lastName = TestUtil.getValueByJPath(responsejson, "/data[0]/last_name");
		String id = TestUtil.getValueByJPath(responsejson, "/data[0]/id");
		String avatar = TestUtil.getValueByJPath(responsejson, "/data[0]/avatar");
		String firstName = TestUtil.getValueByJPath(responsejson, "/data[0]/first_name");
		
		System.out.println(lastName);
		System.out.println(id);
		System.out.println(avatar);
		System.out.println(firstName);
		
		Assert.assertEquals(lastName, "Bluth");
		Assert.assertEquals(Integer.parseInt(id), 1);
		Assert.assertEquals(avatar, "https://s3.amazonaws.com/uifaces/faces/twitter/calebogden/128.jpg");
		Assert.assertEquals(firstName, "George");
		
		//c. All Headers
		Header[] headerArray = closeableHttpResponse.getAllHeaders();
		HashMap<String, String> allHeader = new HashMap<String, String>();
		for(Header header : headerArray) {
			allHeader.put(header.getName(), header.getValue());
		}
		System.out.println("Headers Arrays--->" +allHeader);
		
	}
	
}
